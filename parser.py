from curiosity_one_post import *
import requests

FIRST_POST_INDEX = 124
TIMEOUT = 9000


class PageParser:
    """Парсер списка постов"""

    url = None
    html = None
    soup = None
    cards = None

    def __init__(self, *args, **kwargs):
        if kwargs is not None: self.url = kwargs["url"]
        with open(file='d:\projects\py\curiosity-to-vk\\topics\discovery.html', encoding="utf-8",
                  mode="r") as f: self.html = f.read()
        self.soup = BeautifulSoup(self.html, "lxml")
        self.cards = self.soup.find_all("div", {"class": "articles__item"})


class CardIter:
    """Карточка поста"""

    link = None
    image = None
    title = None

    def __init__(self, data):
        self.data = data
        self.index = len(data)

    def __iter__(self):
        return self

    def __next__(self):
        if self.index == 0:
            raise StopIteration
        self.index = self.index - 1
        return self.data[self.index]


class CardParser:

    FOR_REPLACE_WORDS = "Discovery Channel"
    TO_REPLACE_WORDS = "Любопытство"
    link_sufix = "http://www.discoverychannel.ru/articles/"

    def __init__(self, card):
        self.card_link = card.find("a", {"class": "show-card__link"})["href"],
        self.card_image = card.find("img", {"class": "show-card__pic"})["src"],
        self.card_title = card.find("div", {"class": ["show-card__name title title_h4"]}).text
        self.article_slug = self.card_link[0].split('/')[2]
        self.article_page = requests.get(self.link_sufix + self.article_slug + "/")
        self.article_soup = BeautifulSoup(self.article_page.text, "lxml")
        self.topic_container = self.article_soup.find("div", {"class": "topic__container container"})
        self.text = [top.text for top in self.topic_container.find_all("p")]
        self.stop = "stop"

    def format_text(self):
        data = self.text[0:-1]
        self.text = str()
        for line in data:
            if self.FOR_REPLACE_WORDS in line:
                line.replace(self.FOR_REPLACE_WORDS, self.TO_REPLACE_WORDS)
            self.text = self.text + line
        return self.text

    def img_downloader(self):
        url_sufix = self.card_image[0]
        url_prefix = "http://www.discoverychannel.ru"
        if url_sufix is not None:
            res = requests.get(url_prefix + url_sufix, "b")
            with open(PATH_TO_IMG_ORIGINAL, 'wb') as zero:
                zero.write(res.content)
        else:
            print("Ошибка загрузки изображения")
        print("Скачены изображениея")


def post(text, title):
    vk_session = vk_api.VkApi('79214447344', 'nebova33', api_version="5.67", app_id="5292367",
                              client_secret="bcf090c3bcf090c3bcbe89b988bca0518cbbcf0bcf090c3e400bd450a155ebdf37ebf0f",
                              scope=140488159)
    try:
        vk_session.auth()
    except vk_api.AuthError as error_msg:
        print(error_msg)
        return
    vk = vk_session.get_api()
    upload = vk_api.VkUpload(vk_session)
    photo = upload.photo(
        PATH_TO_IMG_1_COMPOSITE,
        album_id=271361052
    )
    tags = "#ПопулярнаяНаука #ОбыкновенныеГерои"
    post_message = f"""
                    {tags}
                    {text}
                    """
    link1 = "https://github.com/vo0doo"
    link2 = "https://kd.io.net.ru/"
    link3 = "https://mypersweb.firebaseapp.com/"
    link4 = ""
    vk.wall.post(
        owner_id=349494704,
        friends_only=0,
        from_group=0,
        message=str(post_message[:]),
        attachments=f'photo{photo[0]["owner_id"]}_{photo[0]["id"]}, {link1}')
    print(f"НАЗВАНИЕ: {title}" + "\n")


if __name__ == "__main__":
    pp = PageParser(url=None)
    ci = CardIter(data=pp.cards)
    count = 0
    for c in ci:
        count = count + 1
        if count <= FIRST_POST_INDEX:
            continue
        result = False
        while result == False:
            try:
                cp = CardParser(card=c)
                cp.format_text()
                print(f"{count}")
                cp.img_downloader()
                draw(channel_ru=cp.card_title, title_ru="Power by@vo0doo")
                post(text=cp.text, title=cp.card_title)
                result = True
            except Exception as err:
                logger.error(err)
                result = False
        time.sleep(TIMEOUT)
        continue

