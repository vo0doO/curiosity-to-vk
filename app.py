from parser import *


l = logger

def get_args():
    if len(sys.argv) < 3:
        l.warning("Usage: app.py 'интернет или файл' 'путь к файлу[yaml, json или sqlite3]'")
        return False
    else:
        l.info("Параметры упешно загружены")
        return True


def main():
    print("Im main !")


if __name__ == "__main__":

    root_logger = get_logs()  # ПОЛУЧАЕМ ЛОГЕРА
    root_logger.debug('=' * 100)  # ДЕКОРИРУЕМ ЛОГИ

    try:
        if get_args():
            vector = sys.argv[1]
            data_file = sys.argv[2]
            l.info(f"{vector} {data_file}")

    except Exception as error:
        raise SystemExit

    root_logger.debug('=' * 100)  # ДЕКОРИРУЕМ ЛОГИ
