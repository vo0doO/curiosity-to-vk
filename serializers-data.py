import yaml


try:
    from yaml import CLoader as Loader, CDumper as Dumper
except ImportError:
    from yaml import Loader, Dumper


stream = open("D:\Projects\py\curiosity-to-vk\data.yaml", "r")

data = yaml.load(stream)

for model in data:
    if 'curiosity.post' in model.get("model"):
        print(model.get("model"))
